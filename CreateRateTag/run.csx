using System.Net;

public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log)
{
    log.Info($"C# HTTP trigger function processed a request. RequestUri={req.RequestUri}");

    // parse query parameter
    string rateTag = req.GetQueryNameValuePairs()
        .FirstOrDefault(q => string.Compare(q.Key, "ratetag", true) == 0)
        .Value;

   
    string rateValue = req.GetQueryNameValuePairs()
        .FirstOrDefault(q => string.Compare(q.Key, "ratevalue", true) == 0)
        .Value;

    // Set name to query string or body data
    

    return (rateTag == null || rateValue == null)
        ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass ratetag and ratevalue as query string")
        : req.CreateResponse(HttpStatusCode.OK, "Ratetag " + rateTag + " value " + rateValue);
}